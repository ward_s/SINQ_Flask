import json
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, Event
import plotly.graph_objs as go
from scipy import optimize
import numpy as np
import pandas as pd


class dashboard:
    app = []

    def __init__(self,flask,servers):
        self.flask = flask
        app = dash.Dash(__name__, server=flask)
        self.servers = servers

    app.layout = html.Div([
        html.H1(children='Scan Dashboard'),

        html.Div(children='''
            This dashboard allows you to connect to various instruments!
        '''),
        html.Div([
            html.Div([
                html.H2("Instrument"),
                dcc.Dropdown(
                    id='machine-column',
                    options=servers,
                    value=servers[0]["label"]
                ),
                html.Div(
                    dcc.Graph(id='co_graphic',
                              style={
                                  'height': 200
                              },
                              config={
                                  'displayModeBar': False
                              })
                ),
                html.H3('Graph Options'),
                dcc.RadioItems(
                    id='yaxis-type',
                    options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                    value='Linear',
                    labelStyle={'display': 'inline-block'}
                ),
                html.Div([
                    dcc.Checklist(
                        id='af_checklist',
                        options=[{'label': 'Autofit?', 'value': 'AF'}],
                        values=["AF"]
                    ),
                    html.Table(id='fit_data', style={"text-align": "left"})
                ]
                )

            ], style={'width': '24%', 'display': 'inline-block', 'vertical-align': 'top'}),
            html.Div([
                html.H2("Live Graph"),
                html.Div(id='curr_filename', style={"text-align": "right"}),
                html.Div([
                    dcc.Graph(id='plot-graphic'),
                    html.Div(id='intermediate-value', style={'display': 'none'}),
                    dcc.Interval(
                        id='status_interval-component',
                        interval=1.5 * 1000  # in milliseconds
                    )
                ]),
                html.Div([
                    html.Div(id="machine_status", style={"text-align": "right"}),
                ])
            ], style={'width': '74%', 'display': 'inline-block', 'vertical-align': 'top'}),
            html.Div([
                html.H2("Log file"),
                html.Div([
                    html.Table(id='log_table'),
                    # dcc.Interval(
                    #     id='5s_interval-component',
                    #     interval=5*1000 # in milliseconds
                    # )
                ])
            ])
        ])
    ])

    @app.callback(Output('machine_status', 'children'),
                  [Input('machine-column', 'value')],
                  events=[Event('status_interval-component', 'interval')])
    def update_status(xaxis_column):
        return get_mac_status(xaxis_column)

    @app.callback(Output('curr_filename', 'children'),
                  [Input('machine-column', 'value')],
                  events=[Event('status_interval-component', 'interval')])
    def update_filename(host):
        return get_filename(host)

    @app.callback(Output('log_table', 'children'),
                  [Input('machine-column', 'value')])
    def update_log(xaxis_column):
        log = get_mac_log(xaxis_column)
        if log == '\n':
            return 'Unable to show log'
        log = log_to_pandas(log)
        return make_dash_table(log)

    @app.callback(Output('co_graphic', 'figure'),
                  [Input('machine-column', 'value')],
                  events=[Event('status_interval-component', 'interval')])
    def update_count_graph(host):
        mode, data = get_counter(host)
        trace1 = go.Bar(
            y=[mode],
            x=[data[0]],
            name='Current',
            orientation='h',
            # marker=dict(
            #     color='rgba(246, 78, 139, 0.6)',
            #     line=dict(
            #         color='rgba(246, 78, 139, 1.0)',
            #         width=2)
            # )
            marker=dict(
                color='rgba(58, 71, 80, 0.6)',
                line=dict(
                    color='rgba(58, 71, 80, 1.0)',
                    width=2)
            )
        )
        # trace2 = go.Bar(
        #     y=[mode],
        #     x=[data[1]],
        #     name='Total',
        #     orientation='h',
        #     marker=dict(
        #         color='rgba(58, 71, 80, 0.6)',
        #         line=dict(
        #             color='rgba(58, 71, 80, 1.0)',
        #             width=2)
        #     )
        # )
        # data = [trace1, trace2]
        datas = [trace1]
        layout = go.Layout(
            barmode='stack',
            title='Counter %s' % mode,
            yaxis=dict(
                autorange=True,
                showgrid=False,
                zeroline=False,
                showline=False,
                autotick=True,
                ticks='',
                showticklabels=False
            ),
            xaxis=dict(
                range=[0, data[1]]
            ),
            showlegend=False
        )
        return {'data': datas, 'layout': layout}

    def fit_graph(x_data, y_data):
        fitfunc = lambda p, x: p[0] + p[1] * (
            1 / ((p[3] / (2 * np.sqrt(2 * np.log(2)))) * np.sqrt(2 * np.pi))) * np.exp(
            -0.5 * (np.power((x - p[2]) / ((p[3] / (2 * np.sqrt(2 * np.log(2))))), 2)))
        errfunc = lambda p, x, y: fitfunc(p, x) - y
        p0 = [0, 1000 * np.max(y_data), x_data[int(np.round(len(x_data) / 2))],
              3 * (x_data[1] - x_data[0])]  # Initial guess for the parameters
        p1, success = optimize.leastsq(errfunc, p0[:], args=(x_data, y_data))
        x_line = np.linspace(np.array(x_data).min(), np.array(x_data).max(), 100)
        y_line = fitfunc(p1, x_line)
        return dict(
            x_data=x_line,
            y_data=y_line,
            f_data=p1
        )

    @app.callback(Output('fit_data', 'children'), [Input('intermediate-value', 'children')])
    def make_fitdata(all_data):
        all_data = json.loads(all_data)
        fit_data = np.array(all_data.get('FitData').get('f_data'))
        df = pd.DataFrame([fit_data.round(decimals=3)], columns=['Background', 'Amplitude', 'Center', 'Width'])
        return generate_flip_table(df)

    @app.callback(Output('plot-graphic', 'figure'),
                  [Input('machine-column', 'value'), Input('yaxis-type', 'value'),
                   Input('intermediate-value', 'children')])
    def update_graph(xaxis_column, yaxis_type, all_d):

        config_local = [element for element in config.get("hosts") if element['name'] == xaxis_column][0]
        all_d = json.loads(all_d)
        data = all_d.get('DetData')
        fit_data = all_d.get('FitData')

        y_fit = {
            'x': fit_data.get('x_data'),
            'y': fit_data.get('y_data'),
            'name': "Gauss Fit"
        }

        if not config_local['is2D']:
            ret = {
                'data': [go.Scatter(
                    x=data["xdata"],
                    y=data["ydata"],
                    mode='markers',
                    marker={
                        'size': 8,
                        'opacity': 0.5,
                        'line': {'width': 0.5, 'color': 'white'}
                    },
                    name="Data"
                ),
                    y_fit,
                ],
                'layout': go.Layout(
                    xaxis={
                        'title': data["x_label"],
                        'type': 'linear'
                    },
                    yaxis={
                        'title': data["y_label"],
                        'type': 'linear' if yaxis_type == 'Linear' else 'log'
                    },
                    margin={'l': 40, 'b': 40, 't': 10, 'r': 0},
                    hovermode='closest'
                )
            }
        else:
            ret = {
                'data': [go.Heatmap(
                    x=np.arange(data["zdata"][0]),
                    y=np.arange(data["zdata"][1]),
                    z=np.reshape(np.array(data["zdata"][2:]), (data["zdata"][0], data["zdata"][1])),
                    colorscale='Viridis',
                )],
                'layout': go.Layout(
                    xaxis={
                        'title': 'Pixel (x)',
                        'type': 'linear'
                    },
                    yaxis={
                        'title': 'Pixel (y)',
                        'type': 'Linear',
                        'scaleanchor': "x",
                    },
                )
            }
        return ret

    @app.callback(Output('intermediate-value', 'children'),
                  [Input('machine-column', 'value'), Input('af_checklist', 'values')],
                  events=[Event('status_interval-component', 'interval')])
    def clean_data(machine, af):
        # some expensive clean data step
        data = gen_det_data(machine)
        fit_data = {"x_data": [None],
                    "y_data": [None],
                    "f_data": [np.NaN, np.NaN, np.NaN, np.NaN]}

        config_local = [element for element in config.get("hosts") if element['name'] == machine][0]
        if ((len(data['xdata']) > 0 and not config_local['is2D']) and len(af) > 0):
            data['xdata'] = np.arange(len(data['ydata']))
            if not config_local["isPowder"]:
                fit_data = fit_graph(data['xdata'], data['ydata'])
                fit_data["x_data"] = fit_data.get('x_data').tolist()
                fit_data["y_data"] = fit_data.get('y_data').tolist()
                fit_data["f_data"] = fit_data.get('f_data').tolist()
            data['xdata'] = data.get('xdata').tolist()
        temp = {"DetData": data, "FitData": fit_data}
        return json.dumps(temp)  # or, more generally, json.dumps(cleaned_df)