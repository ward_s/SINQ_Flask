from flask import Flask, request, Response, abort
from flask_restful import Resource, Api

import json
import logging
import sys
from os import path, urandom
from collections import OrderedDict

import TelnetSICS

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, Event
import plotly.graph_objs as go
import plotly.offline as offline

from scipy import optimize
import numpy as np
import pandas as pd
import datetime

import telebot

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s:%(lineno)d - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)
config = {}
connections = dict()
offline.init_notebook_mode()


def read_config():
    config_path = path.join(
        path.dirname(sys.argv[0]), "config-host.json")
    logger.info('Reading config: <%s>' % config_path)
    global config
    try:
        with open(config_path, "r", encoding='utf-8') as f:
            config = json.loads(f.read())
    except Exception as e:
        logger.error('%s' % (repr(e)))
        config = {}


def start_conns():
    global connections
    for host in config.get("hosts"):
        if host["connOnStart"]:
            if host["connection"] == "telnet":
                connections[host["name"]] = TelnetSICS.TelnetSICS(host["hostname"], host["port"])
                connections[host["name"]].connect()
                connections[host["name"]].login()


read_config()
start_conns()
server = Flask(__name__)
app = dash.Dash(__name__, server=server)

WEBHOOK_HOST = config.get('WEBHOOK_HOST')
WEBHOOK_PORT = config.get('WEBHOOK_PORT')  # 443, 80, 88 or 8443 (port need to be 'open')
WEBHOOK_LISTEN = '0.0.0.0'  # In some VPS you may need to put here the IP addr
API_TOKEN = config.get('bot_key')

WEBHOOK_SSL_CERT = './webhook_cert.pem'  # Path to the ssl certificate
WEBHOOK_SSL_PRIV = './webhook_pkey.pem'  # Path to the ssl private key

WEBHOOK_URL_BASE = "https://%s:%s" % (WEBHOOK_HOST, WEBHOOK_PORT)
WEBHOOK_URL_PATH = "/%s/" % (API_TOKEN)


bot = telebot.TeleBot(API_TOKEN)


api = Api(server)
servers = [{'label': i, 'value': i} for i in [d['name'] for d in config.get("hosts")]]

class MyException(Exception):
    pass


class DetectorInformation(Resource):
    def get(self, host):
        return gen_det_data(host)


# We can have PUT,DELETE,POST here. But in our API GET implementation is sufficient

api.add_resource(DetectorInformation, '/detector/<string:host>')

# This section deals with the REST requests for addon apps.
@server.route('/detector', methods=['GET'])
def get_detector():
    host_name = request.args.get("host")
    if host_name is not None:
        det_data = gen_det_data(host_name)
        js = json.dumps({'Machines': {'Name': host_name, 'Data': det_data}})
    else:
        js = json.dumps({"Machines": [{'Name': host["name"], 'Data': gen_det_data(host["name"]).tolist()} for host in
                                      config.get("hosts")]})
    resp = Response(js, status=200, mimetype='application/json')
    return resp

@server.route('/status', methods=['GET'])
def api_status():
    host_name = request.args.get("host")
    if host_name is not None:
        text = get_mac_status(host_name)
    else:
        text = ''
    return Response(json.dumps(text), status=200, mimetype='application/json')


@server.route('/log', methods=['GET'])
def api_log():
    host_name = request.args.get("host")
    if request.args.get("num") is None:
        n_len = 50
    else:
        n_len = request.args.get("num")
    if host_name is not None:
        text = get_mac_log(host_name, n_len)
        text = log_to_pandas(text)
        text = text.to_json()
    else:
        text = ''
    return Response(json.dumps(text), status=200, mimetype='application/json')


@server.route('/value', methods=['GET'])
def get_val():
    host_name = request.args.get("host")
    cmds = request.args.get('cmd')
    if (host_name is None) or (cmds is None):
        message = {
            'status': 404,
            'message': 'Not Found: ' + request.url,
        }
        resp = Response(json.dumps(message), status=404, mimetype='application/json')
        return resp
    conn, config_local = check_reconnect(host_name)
    result = {cmds: conn.val(cmds)}
    return Response(json.dumps(result), status=200, mimetype='application/json')

@server.route('/cmd', methods=['GET'])
def send_cmd():
    host_name = request.args.get("host")
    cmds = request.args.get('cmd')
    if (host_name is None) or (cmds is None):
        message = {
            'status': 404,
            'message': 'Not Found: ' + request.url,
        }
        resp = Response(json.dumps(message), status=404, mimetype='application/json')
        return resp
    conn, config_local = check_reconnect(host_name)
    result = {cmds: conn.transact(cmds)}
    return Response(json.dumps(result), status=200, mimetype='application/json')


# This section deals with the telegram app
# Process webhook calls
@server.route(WEBHOOK_URL_PATH, methods=['POST'])
def webhook():
    if request.headers.get('content-type') == 'application/json':
        json_string = request.get_data().decode('utf-8')
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        return ''
    else:
        abort(403)



@bot.message_handler(commands=['start'])
def start_bot(message):
    bot.reply_to(message, 'Hello, ' + message.from_user.first_name)
#
# @bot.message_handler(commands=['plot'])
# def plot_current(message):
#     host = 'morpheus'
#     data = clean_data(host,[])
#     my_graph = update_graph(host, 'Linear', data)
#     salt = urandom(16)
#     my_graph['file'] = salt
#     my_graph['image'] = 'png'
#     offline.iplot(my_graph)


def get_filename(host):
    conn, config_local = check_reconnect(host)
    scan_var = 'iscan'
    try:
        c_file = conn.transact(' '.join([scan_var, 'getfile'])).split('= ')[1][:-1]
    except:
        scan_var = 'xxxscan'
        c_file = conn.transact(' '.join([scan_var, 'getfile'])).split('= ')[1][:-1]
    return c_file


def get_mac_status(host):
    conn, config_local = check_reconnect(host)
    try:
        text = conn.val('status')
    except:
        text = ""
    return text


def get_mac_log(host, n_len=50):
    conn, config_local = check_reconnect(host)
    try:
        text = conn.transact('showlog -l %s' % n_len)
    except:
        text = ""
    return text


def log_to_pandas(text):
    log_lines = text.split('\n')[:-1];
    log_array = [text.split(' ', 3) for text in log_lines]
    log_pandas = pd.DataFrame(log_array, columns=['DateStamp', 'Level', 'Type', 'Message'])
    log_pandas = log_pandas.iloc[::-1]
    log_pandas = log_pandas[log_pandas['DateStamp'].str.contains(datetime.date.today().strftime("%Y")) == True]
    log_pandas['DateStamp'] = pd.to_datetime(log_pandas['DateStamp'], format="%Y-%m-%dT%H-%M-%S.%f")
    return log_pandas


def generate_table(dataframe, max_rows=10):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +
        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )

def generate_flip_table(dataframe, max_rows=10):
    html_row = []
    for i in range(len(dataframe.columns)):
        temp = dataframe[dataframe.columns[i]].values
        html_row.append(
            html.Tr([html.Th(dataframe.columns[i])] + [html.Td(temp[j]) for j in range(min(len(dataframe), max_rows))]))
    return html.Table(html_row)

def make_dash_table(df, max_rows=10):
    ''' Return a dash definitio of an HTML table for a Pandas dataframe '''
    table = []
    for index, row in df.iterrows():
        html_row = []
        for i in range(max_rows):
            html_row.append(html.Td([row[i]]))
        table.append(html.Tr(html_row))
    return table

def check_reconnect(host):
    if host in connections:
        conn = connections[host]
        config_local = [element for element in config.get("hosts") if element['name'] == host][0]
    else:
        config_local = config.get("host_template")
        config_local["hostname"] = host
        name = host.splt(".")
        if len(name) > 1:
            config_local["name"] = name[0]
        connections[host] = TelnetSICS.TelnetSICS(host)
        connections[host].connect()
        connections[host].login()
        conn = connections[host]
    return conn, config_local


def get_counter(host):
    conn, config_local = check_reconnect(host)
    mode = conn.transact('counter mode').split('= ')[1][:-1]

    limit = float(conn.transact('counter preset').split('= ')[1][:-1])
    if mode.lower() == "monitor":
        current = float(conn.transact('counter getmonitor 1').split('= ')[1][:-1])
    else:
        current = float(conn.transact('counter gettime').split('= ')[1][:-1])
    return mode, (current, limit)

def get_point(host):
    conn, config_local = check_reconnect(host)
    scan_var = 'iscan'
    try:
        ydata = conn.transact(' '.join([scan_var, ' getmonitor 1']))
        if len(ydata) == 0:
            raise MyException("iscan does not work, moving to xxxscan")
    except:
        scan_var = 'xxxscan'
        ydata = conn.transact(' '.join([scan_var, 'getmonitor 1']))
    try:
        data = np.array([float(x) for x in (ydata.split(" = { ")[1][:-2]).split(" ")])
    except:
        data = np.array([1])
    return (data.nonzero()[0][-1], len(data))

def gen_det_data(host):
    conn, config_local = check_reconnect(host)

    xdata = None
    ydata = None
    zdata = None

    if config_local["is2D"]:
        zdata = conn.uu_val_comp('hmframe 0')

    if config_local["isPowder"]:
        ydata = conn.uu_val('gethm')
        xdata = conn.get_powder_x(len(ydata))
    else:
        scan_var = 'iscan'
        try:
            ydata = conn.uu_val(' '.join([scan_var, 'uucounts']))
            if len(ydata) == 0:
                raise MyException("iscan does not work, moving to xxxscan")
        except:
            scan_var = 'xxxscan'
            ydata = conn.uu_val(' '.join([scan_var, 'uucounts']))
        scanvars = conn.transact(' '.join([scan_var, 'noscanvar']))
        scanvars = scanvars.split('=')
        scanvars = int(scanvars[1])
        xdata = []
        for i in range(0, scanvars, 1):
            scan_ch = conn.transact(' '.join([scan_var, 'getvarpar', str(i)]))
            scan_ch = scan_ch.split(' = ')
            if float(scan_ch[2]) > 0:
                xdata_s = conn.transact(' '.join([scan_var, 'getvardata', str(i)]))
                xdata_s = xdata_s.split('{ ')
                xdata_s = xdata_s[1:]
                xdata = list(map(lambda x: float(x[:-3]), xdata_s))
                break
    title = conn.transact(' '.join(['lastcommand']))
    if 'ERROR' in title:
        title = conn.transact(' '.join(['scaninfo']))
        title = title.split(',')
        title = title[len(title) - 1][1:]
        xlabel = ""
        ylabel = ""
    else:
        title = title.split('=')
        title = title[1]
        xlabel = scan_ch[0].split('%s.' % scan_var)[1]
        ylabel = 'Counts'
    # Query the result and get cursor.Dumping that data to a JSON is looked by extension
    result = {"xdata": xdata,
              "x_label": xlabel,
              "ydata": ydata,
              "y_label": ylabel,
              "zdata": zdata,
              "title": title}
    return result

def fit_graph(x_data,y_data):
    fitfunc = lambda p, x: p[0] + p[1] * (
        1 / ((p[3] / (2 * np.sqrt(2 * np.log(2)))) * np.sqrt(2 * np.pi))) * np.exp(
        -0.5 * (np.power((x - p[2]) / ((p[3] / (2 * np.sqrt(2 * np.log(2))))), 2)))
    errfunc = lambda p, x, y: fitfunc(p, x) - y
    p0 = [0, 1000 * np.max(y_data), x_data[int(np.round(len(x_data) / 2))],
          3 * (x_data[1] - x_data[0])]  # Initial guess for the parameters
    p1, success = optimize.leastsq(errfunc, p0[:], args=(x_data, y_data))
    x_line = np.linspace(np.array(x_data).min(), np.array(x_data).max(), 100)
    y_line = fitfunc(p1, x_line)
    return dict(
        x_data = x_line,
        y_data = y_line,
        f_data = p1
    )

# This section deals with the scan dashboard
app.layout = html.Div([
    html.H1(children='Scan Dashboard'),

    html.Div(children='''
        This dashboard allows you to connect to various instruments!
    '''),
    html.Div([
        html.Div([
            html.H2("Instrument"),
            dcc.Dropdown(
                id='machine-column',
                options=servers,
                value=servers[0]["label"]
            ),
            html.Div([
                html.H3('Scan Details'),
                dcc.Graph(id='co_graphic',
                          style={
                              'height': 80
                          },
                          config={
                              'displayModeBar': False
                          }),
                dcc.Graph(id='pt_graphic',
                          style={
                              'height': 80
                          },
                          config={
                              'displayModeBar': False
                          })]
            ),
            html.Div([
                html.H3('Graph Options'),
                html.Div([
                    html.H4('Display Options'),
                    dcc.RadioItems(
                        id='yaxis-type',
                        options=[{'label': i, 'value': i} for i in ['Linear', 'Log']],
                        value='Linear',
                        labelStyle={'display': 'inline-block'}
                    ),
                dcc.Checklist(
                    id='af_checklist',
                    options=[{'label': 'Autofit?', 'value': 'AF'}],
                    values=["AF"]
                ),
                ], style={'width': '44%', 'float': 'left'}),
                html.Div([

                    html.H4('Fit Results'),
                    html.Table(id='fit_data', style={"text-align": "left"})
                ], style={'width': '54%', 'float': 'right'})
            ]),
        ], style={'width': '24%', 'display': 'inline-block', 'vertical-align': 'top'}),
        html.Div([
            html.H2("Live Graph"),
            html.Div(id='curr_filename', style={"text-align": "right"}),
            html.Div([
                dcc.Graph(id='plot-graphic'),
                # This is a secret hiding place for the data :-)
                html.Div(id='intermediate-value', style={'display': 'none'}),
                dcc.Interval(
                    id='status_interval-component',
                    interval=1.5 * 1000  # in milliseconds
                )
            ]),
            html.Div([
                html.Div(id="machine_status", style={"text-align": "right"}),
            ])
        ], style={'width': '74%', 'display': 'inline-block', 'vertical-align': 'top'}),
        html.Div([
            html.Div([
                html.H2("Current Positions"),
                html.Div(
                    html.Table(id="motor_table"))
            ],style={'width':'32%','float':'left'}),
            html.Div([
                html.H2("Log file"),
                html.Div([
                    html.Table(id='log_table')
                ])
            ],style={'width':'64%','float':'right'})
        ]),
    ])
])


@app.callback(Output('intermediate-value', 'children'), [Input('machine-column', 'value'),Input('af_checklist', 'values')],
              events=[Event('status_interval-component', 'interval')])
def clean_data(machine, af):
    # some expensive clean data step
    data = gen_det_data(machine)
    fit_data= {"x_data": [None],
               "y_data": [None],
               "f_data": [np.NaN, np.NaN, np.NaN, np.NaN]}

    config_local = [element for element in config.get("hosts") if element['name'] == machine][0]
    if ((len(data['xdata']) > 0 and not config_local['is2D']) and len(af) > 0):
        if not config_local["isPowder"]:
            data['xdata'] = np.arange(len(data['ydata']))
            fit_data = fit_graph(data['xdata'], data['ydata'])
            fit_data["x_data"] = fit_data.get('x_data').tolist()
            fit_data["y_data"] = fit_data.get('y_data').tolist()
            fit_data["f_data"] = fit_data.get('f_data').tolist()
        data['xdata'] = data.get('xdata').tolist()
    temp = {"DetData":data,"FitData":fit_data}
    return json.dumps(temp) # or, more generally, json.dumps(cleaned_df)

@app.callback(Output('motor_table','children'),
              [Input('machine-column', 'value')],
              events=[Event('status_interval-component', 'interval')])
def make_motor_table(host):
    conn, config_local = check_reconnect(host)
    if conn.motor_list is None:
        conn.get_motors()
    motors = conn.motor_list
    c_val = [round(float(conn.val(motor)),3) for motor in motors]
    c_ll = [round(float(conn.val(motor+ " softlowerlim")),3) for motor in motors]
    c_ul = [round(float(conn.val(motor+ " softupperlim")),3) for motor in motors]
    c_fi = [float(conn.val(motor+ " fixed")) for motor in motors]
    c_tp = [conn.val(motor + " target") for motor in motors]
    for i, val in enumerate(c_tp):
        if 'ERROR' in val:
            c_tp[i] = conn.val(motors[i] + " targetposition")
        if "not found" in c_tp[i]:
            c_tp[i] = np.NAN
        else:
            c_tp[i] = round(float(c_tp[i]),3)
    df = pd.DataFrame({"Motors": motors, 'Value': c_val, 'Soft Lower':c_ll, 'Soft Upper':c_ul, 'Fixed':c_fi, 'Target':c_tp})
    return generate_table((df.iloc[:,[1,5,4,2,3,0]]).sort_values(["Motors"]),len(df))

@app.callback(Output('machine_status', 'children'),
              [Input('machine-column', 'value')],
              events=[Event('status_interval-component', 'interval')])
def update_status(xaxis_column):
    return get_mac_status(xaxis_column)


@app.callback(Output('curr_filename', 'children'),
              [Input('machine-column', 'value')],
              events=[Event('status_interval-component', 'interval')])
def update_filename(host):
    return get_filename(host)


@app.callback(Output('log_table', 'children'),
              [Input('machine-column', 'value')])
def update_log(xaxis_column):
    log = get_mac_log(xaxis_column)
    if log == '\n':
        return 'Unable to show log'
    log = log_to_pandas(log)
    return generate_table(log)


@app.callback(Output('co_graphic', 'figure'),
              [Input('machine-column', 'value')],
              events=[Event('status_interval-component', 'interval')])
def update_count_graph(host):
    mode, data = get_counter(host)
    trace1 = go.Bar(
        y=[mode],
        x=[data[0]],
        name='Current',
        orientation='h',
        marker=dict(
            color='rgba(58, 71, 80, 0.6)',
            line=dict(
                color='rgba(58, 71, 80, 1.0)',
                width=2)
        )
    )
    datas = [trace1]
    layout = go.Layout(
        barmode='stack',
        title='Counter %s' % mode,
        yaxis=dict(
            autorange=True,
            showgrid=False,
            zeroline=False,
            showline=False,
            autotick=True,
            ticks='',
            showticklabels=False
        ),
        xaxis=dict(
            range=[0, data[1]]
        ),
        showlegend=False,
        margin=go.Margin(
            l=5,
            r=10,
            b=15,
            t=40,
            pad=0
        )
    )
    return {'data': datas, 'layout': layout}

@app.callback(Output('pt_graphic', 'figure'),
              [Input('machine-column', 'value')],
              events=[Event('status_interval-component', 'interval')])
def update_points_graph(host):
    data = get_point(host)
    trace1 = go.Bar(
        y="Points",
        x=[data[0]],
        name='Current',
        orientation='h',
        marker=dict(
            color='rgba(58, 71, 80, 0.6)',
            line=dict(
                color='rgba(58, 71, 80, 1.0)',
                width=2)
        )
    )
    datas = [trace1]
    layout = go.Layout(
        barmode='stack',
        title='Scan Points',
        yaxis=dict(
            autorange=True,
            showgrid=False,
            zeroline=False,
            showline=False,
            autotick=True,
            ticks='',
            showticklabels=False
        ),
        xaxis=dict(
            range=[0, data[1]]
        ),
        showlegend=False,
        margin=go.Margin(
            l=5,
            r=10,
            b=15,
            t=40,
            pad=0
        ),
    )
    return {'data': datas, 'layout': layout}

@app.callback(Output('fit_data','children'),[Input('intermediate-value', 'children')])
def make_fitdata(all_data):
    all_data = json.loads(all_data)
    fit_data = np.array(all_data.get('FitData').get('f_data'))
    df = pd.DataFrame([fit_data.round(decimals=3)], columns=['Background', 'Amplitude', 'Center', 'Width'])
    return generate_flip_table(df)

@app.callback(Output('plot-graphic', 'figure'),
              [Input('machine-column', 'value'), Input('yaxis-type', 'value'),Input('intermediate-value', 'children')])
def update_graph(xaxis_column, yaxis_type, all_d):

    config_local = [element for element in config.get("hosts") if element['name'] == xaxis_column][0]
    all_d = json.loads(all_d)
    data = all_d.get('DetData')
    fit_data = all_d.get('FitData')

    y_fit = {
                'x': fit_data.get('x_data'),
                'y': fit_data.get('y_data'),
                'name': "Gauss Fit"
            }

    if not config_local['is2D']:
        ret = {
            'data': [go.Scatter(
                x=data["xdata"],
                y=data["ydata"],
                mode='markers',
                marker={
                    'size': 8,
                    'opacity': 0.5,
                    'line': {'width': 0.5, 'color': 'white'}
                },
                name="Data"
            ),
                y_fit,
            ],
            'layout': go.Layout(
                xaxis={
                    'title': data["x_label"],
                    'type': 'linear'
                },
                yaxis={
                    'title': data["y_label"],
                    'type': 'linear' if yaxis_type == 'Linear' else 'log'
                },
                margin={'l': 40, 'b': 40, 't': 10, 'r': 0},
                hovermode='closest'
            )
        }
    else:
        ret = {
            'data': [go.Heatmap(
                x=np.arange(data["zdata"][0]),
                y=np.arange(data["zdata"][1]),
                z=np.reshape(np.array(data["zdata"][2:]), (data["zdata"][0], data["zdata"][1])),
                colorscale='Viridis',
            )],
            'layout': go.Layout(
                xaxis={
                    'title': 'Pixel (x)',
                    'type': 'linear'
                },
                yaxis={
                    'title': 'Pixel (y)',
                    'type': 'Linear',
                    'scaleanchor' : "x",
                },
            )
        }
    return ret

if __name__ == '__main__':
    # Remove webhook, it fails sometimes the set if there is a previous webhook
    bot.remove_webhook()

    # Set webhook
    bot.set_webhook(url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH,
                    certificate=open(WEBHOOK_SSL_CERT, 'r'))

    server.run(port=WEBHOOK_PORT,ssl_context=(WEBHOOK_SSL_CERT, WEBHOOK_SSL_PRIV),debug=True)

